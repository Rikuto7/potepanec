RSpec.describe "Products", type: :request do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 5, name: 'related_products', taxons: [taxon]) }

  before do
    get potepan_product_path(product.id)
  end

  describe "GET /products" do
    it "リクエストが成功する" do
      expect(response).to have_http_status(200)
    end

    it '商品詳細のviewが表示される' do
      expect(response).to render_template :show
    end

    it '商品名が表示される' do
      expect(response.body).to include product.name
    end

    it '金額が表示される' do
      expect(response.body).to include product.display_price.to_s
    end

    it "商品の説明が表示される" do
      expect(response.body).to include product.description
    end

    it '関連商品が表示される' do
      related_products.each do |related_product|
        expect(response.body).to include related_product.name
      end
    end

    it '関連商品を4件取得' do
      expect(controller.instance_variable_get("@related_products").length).to eq 4
    end
  end
end
