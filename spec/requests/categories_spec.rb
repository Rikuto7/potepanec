RSpec.describe "Categories", type: :request do
  let(:taxon) { create(:taxon, parent: taxonomy.root, taxonomy: taxonomy) }
  let(:taxonomy) { create(:taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }

  before do
    get potepan_category_path(taxon.id)
  end

  describe "GET /categories" do
    it "リクエストが成功する" do
      expect(response).to have_http_status(200)
    end

    it "カテゴリー商品の一覧が表示される" do
      expect(response).to render_template :show
    end

    it "taxonが表示される" do
      expect(response.body).to include taxon.name
    end

    it "taxonmiesが表示される" do
      expect(response.body).to include taxonomy.name
    end

    it '商品名が表示される' do
      expect(response.body).to include product.name
    end

    it '金額が表示される' do
      expect(response.body).to include product.display_price.to_s
    end
  end
end
