RSpec.describe 'Product_decorator', type: :model do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:related_products) { create(:product, name: 'related_product', taxons: [taxon]) }
  let!(:norelated_products) { create(:product) }

  it "関連商品の取得" do
    expect(product.related_products).to include related_products
  end

  it "メインの商品が関連商品に含まれない" do
    expect(product.related_products).not_to include product
  end

  it "関連していない商品が取得されていない" do
    expect(product.related_products).not_to include norelated_products
  end
end
