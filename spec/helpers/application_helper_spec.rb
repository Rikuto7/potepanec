RSpec.describe ApplicationHelper, type: :helper do
  describe "titleのテスト" do
    context 'titleの引数がある場合' do
      it { expect(title("Ruby on Rails")).to eq "Ruby on Rails - BIGBAG Store" }
    end

    context 'titleの引数が空文字の場合' do
      it { expect(title("")).to eq "BIGBAG Store" }
    end

    context 'titleの引数がnilの場合' do
      it { expect(title(nil)).to eq "BIGBAG Store" }
    end
  end
end
