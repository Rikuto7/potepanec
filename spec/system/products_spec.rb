RSpec.describe "Products", type: :system do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, name: 'related_product', taxons: [taxon]) }

  before do
    visit potepan_product_path(product.id)
  end

  it "一覧ページへの遷移" do
    expect(page).to have_link "一覧ページへ戻る"
    click_link "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(taxon.id)
  end

  it "商品詳細ページへの遷移" do
    within '.relatedProducts' do
      expect(page).to have_content related_product.name
      expect(page).to have_content related_product.display_price
    end
    first('.categoriesLink').click
    expect(current_path).to eq potepan_product_path(related_product.id)
  end
end
