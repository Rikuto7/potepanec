RSpec.describe "Categories", type: :system do
  let(:other_taxon) { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  let(:taxon) { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  let(:taxonomy) { create(:taxonomy) }
  let!(:taxons) { create(:taxon, name: 'bags', parent: taxonomy.root) }
  let!(:other_taxons) { create(:taxon, name: 'shirts', parent: taxonomy.root) }
  let!(:product) { create(:product, taxons: [taxon]) }

  before do
    visit potepan_category_path(taxon.id)
  end

  it 'カテゴリーをクリックするとカテゴリページが表示される' do
    within '.mainContent' do
      expect(page).to have_content taxonomy.name
      expect(page).to have_content taxons.name
      expect(page).to have_content other_taxons.name
    end
    click_on other_taxons.name
    expect(current_path).to eq potepan_category_path(other_taxons.id)
  end

  it '商品をクリックすると商品詳細ページへ画面遷移' do
    within '.productCaption' do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
    end
    first('.productsLink').click
    expect(current_path).to eq potepan_product_path(product.id)
  end
end
