class Potepan::ProductsController < ApplicationController
  RELATED_PRODUCT_NUM = 4
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products.limit(RELATED_PRODUCT_NUM)
  end
end
